Bateria de testes da calculadora

Narrative:
In order to fazer as 4 operações básicas
As a operação da calculadora
I want to obter o resultado da operação

Scenario: Soma de dois inteiros
Given uma operação
When peço para somar 2 e -2
Then a soma é 0

Given uma operação
When peço para somar -2 e -2
Then a soma é -4

Examples:
|op1|op2|r|
|2|-2|0|
|-2|-2|-4|
|0|2|2|

Scenario: Soma de valores grandes
Given uma operação
When peço para somar 2000 e 2
Then lança uma exceção

Given uma operação 
When peço para somar 2 e 2000
Then lança uma exceção