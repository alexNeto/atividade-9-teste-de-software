package com.atividade9.testesoftware.atv9.aulaTest;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.jbehave.core.annotations.Alias;
import org.jbehave.core.annotations.Given;
import org.jbehave.core.annotations.Named;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;

import com.atividade9.testesoftware.atv9.aula.Calculadora;

public class CalcSteps {
	private Calculadora calculadora;
	private int r;
	private Exception error;

	@Given("uma operação")
	public void setUp() {
		calculadora = new Calculadora();
		error = null;
	}

	@When("peço para somar <op1> e <op2>") // usando tabela
	@Alias("peço para somar $op1 e $op1") // usando valor
	// a anotação @Named é usada para indicar qual variável recebe o parâmetro
	public void somaTest(@Named("op1") int x, @Named("op2") int y) {
		try {
			this.r = calculadora.soma(x, y);
		} catch (Exception e) {
			error = e;
		}
	}

	@Then("a soma é <r>")
	@Alias("a soma é $r")
	public void somaResposta(@Named("r") int r) {
		assertEquals(this.r, r);
	}

	@Then("lança uma exceção")
	public void thenItWillRaiseAnError() {
		assertNotNull(error);
	}
}