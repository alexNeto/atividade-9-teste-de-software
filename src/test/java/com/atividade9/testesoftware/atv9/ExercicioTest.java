package com.atividade9.testesoftware.atv9;

import static org.junit.Assert.assertEquals;

import java.util.Arrays;
import java.util.Collection;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

@RunWith(Parameterized.class)
public class ExercicioTest {

	private Exercicios exercicios;
	private Integer saida;
	private Integer entrada;

	@Before
	public void initialize() {
		/**
		 * GIVEN a calculator
		 */
		exercicios = new Exercicios();
	}

	public ExercicioTest(Integer entreda, Integer saida) {
		this.entrada = entreda;
		this.saida = saida;
	}

	@Parameterized.Parameters
	public static Collection<Object[]> parametros() {
		/**
		 * | entrada | saida |
		 * |	1	 |	-1	 |
		 * |	3	 |	-3	 |
		 * |   -10	 |	-10	 |
		 */
		return Arrays.asList(new Object[][] { { 1, -1 }, { 3, -3 }, {-10, -10} });
	}

	@Test
	public void test1() throws Exception {
		/**
		 * WHEN I ask the negative for <entrada>
		 * THEN it returns the <saida>
		 */
		assertEquals(this.saida, exercicios.makeNegative(this.entrada));
	}

}
