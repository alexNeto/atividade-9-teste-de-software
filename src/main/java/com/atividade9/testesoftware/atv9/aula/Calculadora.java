package com.atividade9.testesoftware.atv9.aula;

public class Calculadora {
	public int soma(Integer x, Integer y) {
		if (x > 1000 || y > 1000)
			throw new RuntimeException();
		return x + y;
	}
}
