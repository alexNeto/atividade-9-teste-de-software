package com.atividade9.testesoftware.atv9;

public class Exercicios {

	/**
	 * 1 - O BDD é focaddo mais na regra de negócio enquanto que o TDD é mais focado
	 * nas funcionalidades das classes e métodos. A diferença é como é feito o teste
	 */

	/**
	 * 2 - Por que o BDD é voltado para a regra de negócio, enquanto que o TDD é
	 * voltado para comportamento. Como a conexão do banco de dados não é relevante
	 * para a regra de negócio.
	 */

	public Integer makeNegative(Integer a) {
		if (a < 0) {
			return a;
		}
		return a * -1;
	}
	
	/**
	 * 4 - O JBehave mapeia etapas textuais para métodos Java via CandidateSteps.
	 * O desenvolvedor precisa apenas fornecer métodos anotados que correspondam, 
	 * por padrões de regex, às etapas textuais. Para criar instâncias de CandidateSteps,
	 * usasse uma instância de InjectableStepsFactory
	 */

	/**
	 * 5 - A notação <> é utilizada em testes parametrizados, enquanto a notação $ é 
	 * utilizada em testes comuns. Nesse caso, as duas notações dão a opção de o teste 
	 * ter seus parâemtros de um cenário parametrizado ou de um cenário comum.
	 */
	
	
}
